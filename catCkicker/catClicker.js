const cat1Name = document.getElementById("cat1Name");
const cat2Name = document.getElementById("cat2Name");
const cat1Pic = document.getElementById("cat1Pic");
const cat2Pic = document.getElementById("cat2Pic");
const clickNumberElt = document.getElementById("clickNumberElt");
var clickNumber = 0;

cat1Name.innerHTML = catNames.cat1;
cat2Name.innerHTML = catNames.cat2;

const clickNumberDisplay = function () {
  clickNumber++;
  clickNumberElt.innerHTML = clickNumber;
};

if (cat1Pic) {
  cat1Pic.addEventListener('click', clickNumberDisplay);
}
if (cat2Pic) {
  cat2Pic.addEventListener('click', clickNumberDisplay);
}

